#include "Player.h"



Player::Player(SDL_Renderer* _renderer)
{
	renderer = _renderer;
	image = IMG_Load("asd.bmp");
	texture = SDL_CreateTextureFromSurface(renderer, image);
}


Player::~Player()
{
}

void Player::Render()
{
	SDL_Rect dstrect = { x, y, 100, 100 };
	SDL_RenderCopy(renderer, texture, NULL, &dstrect);
	//SDL_RenderCopy(renderer, texture, NULL, NULL);
	SDL_RenderPresent(renderer);
}

void Player::Clean()
{
	SDL_DestroyTexture(texture);
	SDL_FreeSurface(image);
	SDL_DestroyRenderer(renderer);
}

void Player::Update(SDL_Event event)
{
	if (event.type = SDL_KEYDOWN )
	{
		if (event.key.keysym.sym == SDLK_LEFT)
			x-= speed;
		if (event.key.keysym.sym == SDLK_RIGHT)
			x+= speed;
		if (event.key.keysym.sym == SDLK_UP)
			y-= speed;
		if (event.key.keysym.sym == SDLK_DOWN)
			y+= speed;
	}
}