#include "Render.h"

Render::Render() {
}

SDL_Surface* Render::OnLoad(char* File) {
	SDL_Surface* Surf_Return = NULL;
	Surf_Return = SDL_LoadBMP(File);

	return Surf_Return;
}

bool Render::OnDraw(SDL_Renderer* renderer,SDL_Surface* Surf_Src, int X, int Y) {
	if ( Surf_Src == NULL) {
		return false;
	}
	SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, Surf_Src);
	SDL_Rect DestR;

	DestR.x = X;
	DestR.y = Y;

	SDL_RenderCopy(renderer, texture, NULL, NULL);

	return true;
}

