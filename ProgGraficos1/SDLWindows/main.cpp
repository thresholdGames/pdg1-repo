#include <SDL.h>
#include <SDL_image.h>
#include "Player.h"

void WindowClear(SDL_Renderer* render)
{
	SDL_RenderClear(render);
}

int main(int argc, char ** argv)
{
	bool quit = false;
	SDL_Event event;

	SDL_Init(SDL_INIT_VIDEO);
	IMG_Init(IMG_INIT_JPG);

	SDL_Window * window = SDL_CreateWindow("Window",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, 0);

	SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);

	Player* player = new Player(renderer);

	while (!quit)
	{
		SDL_WaitEvent(&event);

		switch (event.type)
		{
		case SDL_QUIT:
			quit = true;
			break;
		}

		player->Render();
		player->Update(event);
		WindowClear(renderer);
	}

	player->Clean();
	SDL_DestroyWindow(window);

	IMG_Quit();
	SDL_Quit();

	return 0;
}