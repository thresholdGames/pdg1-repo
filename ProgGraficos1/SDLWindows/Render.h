#ifndef SURFACE_H
#define SURFACE_H

#include <SDL.h>
#include "App.h"

class Render {

private:
	App* app;

public:
	Render();

public:
	static SDL_Surface* OnLoad(char* File);
	static bool OnDraw(SDL_Renderer* renderer, SDL_Surface* Surf_Src, int X, int Y);
};


#endif // !1
