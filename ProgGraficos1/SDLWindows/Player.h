#include <SDL.h>
#include <SDL_image.h>

class Player
{

private:
	float x, y = 0;
	float speed = 10;
	SDL_Renderer * renderer;
	SDL_Surface * image;
	SDL_Texture * texture;

public:
	Player(SDL_Renderer* window);
	~Player();

	void Update(SDL_Event event);
	void Move();
	void Render();
	void Clean();
};

